#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	ofHideCursor();
	myGame		= new cGame();
	myGame->setup(1280, 720);
	
	myBike1Thread	= new cBikeThread(*myGame,1,1000);
	myBike1Thread->startThread(true, false);
	myBike2Thread	= new cBikeThread(*myGame,2,1200);
	myBike2Thread->startThread(true, false);
	
	m_infoText	= true;
}

//--------------------------------------------------------------
void ofApp::update(){
	myGame->update(m_infoText);
}

//--------------------------------------------------------------
void ofApp::draw(){

	myGame->draw();
	
	ofSetColor(255);
           
    if(m_infoText) {
        stringstream ss;
        ss << "Framerate: " << ofToString(ofGetFrameRate(),0) << "\n";
        ss << "(f): Toggle Fullscreen"<<endl<<"(t): Info Text"<< endl;
        
        stringstream ss2;
        ss2 << "(n): Start new game"<<endl<<"(i): Intro"<< endl;
        
        ofDrawBitmapString(ss.str().c_str(), 20, 20);
        ofDrawBitmapString(ss2.str().c_str(), 600, 20);
    }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	switch(key) {
		case 'f':
			ofToggleFullscreen();
			break;
		case 't':
			if(m_infoText){
				m_infoText	= false;
			}else{
				m_infoText	= true;
			}
			break;
		case 'n':
			myGame->startNewGame();
			break;
		case 'i':
			myGame->startIntro();
			break;
	}

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
	switch(key) {
		case 'q':
			myGame->bikeEvent(1);
			break;
		case 's':
			myGame->bikeEvent(2);
			break;
			break;
	}
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

//--------------------------------------------------------------
void ofApp::exit(){
	myBike1Thread->stopThread();
}
