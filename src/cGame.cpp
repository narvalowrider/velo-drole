#include "cGame.h"

//--------------------------------------------------------------
void cGame::initialization(){
	player1			= new cPlayer();
	player1->setup();
	player2			= new cPlayer();
	player2->setup();
	m_player1Pos	= 0;
	m_player2Pos	= 0;
	m_gameMode		= 0;	/*
								0	- Intro loop
								1	- Game with progress bar
								2	- Results
							*/
	m_countTimer    = 0;
}
	
//--------------------------------------------------------------
void cGame::setup(int width, int height){
	font_winner.loadFont("Resistance_is_Futile.ttf", 54);
	m_infoText		= false;
	m_distance		= 200;		//	distance to pedal !
	m_screenWidth	= width;
	m_screenHeight	= height;
	m_screenBorder	= 100;

	myIntroVideo	= new ofVideoPlayer();
	myIntroVideo->loadMovie("velodrole_intro.mov");
	myIntroVideo->play();
	this->initialization();
}

//--------------------------------------------------------------
void cGame::update(bool infoText){
	m_infoText	= infoText;
	
	switch (m_gameMode){
		case 0:
			myIntroVideo->update();
			break;
		case 1:
			this->updatePlayerPosition();
			m_player1Pos	= player1->getPlayerPosition();
			m_player2Pos	= player2->getPlayerPosition();
			break;
		case 2:
			break;
	}
	
}

//--------------------------------------------------------------
void cGame::draw(){
	this->drawBackground();
	float	pp1 = 0.00;
	float	pp2 = 0.00;
	pp1			= (m_player1Pos*100.00)/m_distance;
	pp2			= (m_player2Pos*100.00)/m_distance;
	
	switch (m_gameMode){
		case 0:
			this->playIntroVideo();
			break;
		case 1:
			this->drawProgressBar(1,pp1);
			this->drawProgressBar(2,pp2);
			break;
		case 2:
			this->drawProgressBar(1,pp1);
			this->drawProgressBar(2,pp2);
		
			int winner = 0;
			if(pp1 > pp2){
				winner = 1;
			}else{
				winner = 2;
			}
			this->drawWinner(winner);

			break;
			
	}
	ofSetColor(255);
	if(m_infoText) {
        stringstream ss3;

		ss3 << "Game mode: " << ofToString(m_gameMode,0) << "\n";
        ss3 << "Player1 position: " << ofToString(m_player1Pos,0) << " m" << "\n";
        ss3 << "Player2 position: " << ofToString(m_player2Pos,0) << " m" << endl;
        
        ofDrawBitmapString(ss3.str().c_str(), 300, 20);
    }
}

//--------------------------------------------------------------
void cGame::drawBackground(){
	ofBackground(0,0,0);

}

//--------------------------------------------------------------
void cGame::drawWinner(int winner){
	int countdownAlpha  = 0;
	int countDuration   = 900;
	int minAlpha		= 50;
	
	if((ofGetElapsedTimeMillis()-m_countTimer)>=countDuration){
			m_countTimer = ofGetElapsedTimeMillis();
	}
	
	countdownAlpha = (((ofGetElapsedTimeMillis()-m_countTimer)*(255-minAlpha))+minAlpha)/countDuration;
	
	switch(winner){
		case 1:
			ofSetColor(255,255,255,255-countdownAlpha);
			font_winner.drawString("WINNER !", m_screenWidth-360, 92);
			
			ofSetColor(255,255,255,255-countdownAlpha);
			font_winner.drawString("LOSER !", 30, 130+92);
			break;
			
		case 2:
			ofSetColor(255,255,255,255-countdownAlpha);
			font_winner.drawString("WINNER !", 30, 130+92);
			
			ofSetColor(255,255,255,255-countdownAlpha);
			font_winner.drawString("LOSER !", m_screenWidth-360, 92);
			break;
	}
}

//--------------------------------------------------------------
void cGame::drawProgressBar(int player,float progression){
	int	progressBarHeight		= 130;

	int	progressBarWidth		= progression*(m_screenWidth)/100.00;
	
	if((progression>=0.00)&&(progression<=100.00)){
		switch(player){
			case 1:
				ofSetColor(198,31,1);
				ofRect(0,0,progressBarWidth,progressBarHeight);
				player1->draw();
				break;
			case 2:
				ofSetColor(1,159,198);
				ofRect(m_screenWidth-progressBarWidth,progressBarHeight,progressBarWidth,progressBarHeight);
				player2->draw();
				break;
		}
	}else{
		cout << "[error] Invalid progress" << endl;
	}
}

//--------------------------------------------------------------
void cGame::updatePlayerPosition(){
	player1->update();
	player2->update();
	
	if(player1->getPlayerPosition()>=m_distance){
		m_gameMode = 2;
	}else{
		if(player2->getPlayerPosition()>=m_distance){
			m_gameMode = 2;
		}	
	}
}

//--------------------------------------------------------------
void cGame::playIntroVideo(){
		myIntroVideo->draw(0,0);
}

//--------------------------------------------------------------
void cGame::bikeEvent(int biker){
	if(m_gameMode == 1){
		switch(biker){
			case 1:
				player1->setNewPosition();
				break;
			case 2:
				player2->setNewPosition();
				break;
		}
	}
}

//--------------------------------------------------------------
void cGame::startNewGame(){
	this->initialization();
	m_gameMode	= 1;
}

//--------------------------------------------------------------
void cGame::startIntro(){
	myIntroVideo->setPosition(0);
	ofSleepMillis(100);
	m_gameMode	= 0;
}

