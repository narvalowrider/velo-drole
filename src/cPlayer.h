#pragma once

#include "ofMain.h"

class cPlayer : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();
		
		void setNewPosition();
		int	 getPlayerPosition();
	private:
		int  m_playerPosition;
};
