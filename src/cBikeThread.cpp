#include "cBikeThread.h"

//--------------------------------------------------------------
cBikeThread::cBikeThread(cGame& anyGame,int bikeNumber,int interval){
	myGame			= &anyGame;
	wiringPiSetup();
	m_newFlash1		= false;
	m_oldFlash1		= false;
	m_newFlash2		= false;
	m_oldFlash2		= false;
	m_chrono		= ofGetElapsedTimeMillis();
	m_bikeNumber		= bikeNumber;
	m_interval		= interval;
	m_sleepDuration		= 1; // milliseconds
	
	m_pinBike1		= 25;	// labelled 26
	m_pinBike2		= 22;	// labelled 22
	pinMode(m_pinBike1,INPUT);
	pinMode(m_pinBike2,INPUT);
}

//--------------------------------------------------------------
void	cBikeThread::threadedFunction(){
	while(isThreadRunning()) {
		if((ofGetElapsedTimeMillis()-m_chrono)>=m_interval){
			this->checkIfNewFlash();
		}
		ofSleepMillis(m_sleepDuration);
	}
}

//--------------------------------------------------------------
void	cBikeThread::checkIfNewFlash(){
	int tmp = 2;
	if((m_bikeNumber==1)||(m_bikeNumber==2)){
		switch(m_bikeNumber){
				case 1:
					tmp	= digitalRead(m_pinBike1);
					switch(tmp){
						case 0:
							m_newFlash1	= false;
							break;
						case 1:
							m_newFlash1	= true;
							break;
						case 2:
							m_newFlash1	= m_oldFlash1;
							break;
					}
					if(m_newFlash1 != m_oldFlash1){
						if(m_newFlash1 == true){
							myGame->bikeEvent(m_bikeNumber);
						}
						m_oldFlash1	= m_newFlash1;
					}
					break;
				case 2:
					tmp	= digitalRead(m_pinBike2);
					switch(tmp){
						case 0:
							m_newFlash2	= false;
							break;
						case 1:
							m_newFlash2	= true;
							break;
						case 2:
							m_newFlash2	= m_oldFlash2;
							break;
					}
					if(m_newFlash2 != m_oldFlash2){
						if(m_newFlash2 == true){
							myGame->bikeEvent(m_bikeNumber);
						}
						m_oldFlash2	= m_newFlash2;
					}
					break;
		}
	}
}
