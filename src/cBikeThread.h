#pragma once
#include "ofMain.h"
#include "cGame.h"
#include "wiringPi.h"

class cBikeThread : public ofThread{

	public:
		cBikeThread(cGame&,int,int);
		void	threadedFunction();
	
	private:
		bool	m_newFlash1;
		bool	m_oldFlash1;
		bool	m_newFlash2;
		bool	m_oldFlash2;
		int		m_chrono;
		int		m_interval;
		int		m_bikeNumber;
		int		m_sleepDuration;
		
		int		m_pinBike1;
		int		m_pinBike2;
		
		cGame	*myGame;
		
		void	checkIfNewFlash();
};
