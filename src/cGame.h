#pragma once

#include "ofMain.h"
#include "cPlayer.h"

class cGame : public ofBaseApp{

	public:
		void setup(int,int);
		void update(bool);
		void draw();
		void bikeEvent(int);
		void startNewGame();
		void startIntro();
		
	private:
		ofTrueTypeFont	font_winner;
		bool			m_infoText;
		int				m_distance;
		int				m_screenHeight;
		int				m_screenWidth;
		int				m_screenBorder;
		int				m_gameMode;
		int				m_player1Pos;
		int				m_player2Pos;
		int				m_countTimer;
		
		ofVideoPlayer	*myIntroVideo;
		cPlayer			*player1;
		cPlayer			*player2;

		void drawBackground();
		void drawWinner(int);
		void drawProgressBar(int,float);
		void updatePlayerPosition();
		void playIntroVideo();
		void initialization();
};
